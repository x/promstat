.. _next-steps:

Next steps
~~~~~~~~~~

Your OpenStack environment now includes the promstat service.

To add additional services, see
https://docs.openstack.org/project-install-guide/ocata/.
